type InnerObj = {
    data1: number,
    data2: string,
    data3: {
        id: number,
        name: string
    },
    data4: {
        id: string
    }
}

export {
    InnerObj
};
